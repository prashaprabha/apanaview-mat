import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
               component: DashboardComponent
            },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'agents',
                loadChildren: './accounts/agent/agent.module#AgentModule'
            },{
                path: 'suppliers',
                loadChildren: './accounts/supplier/supplier.module#SupplierModule'
            },
            {
                path: 'sa',
                loadChildren: './accounts/sa/sa.module#SaModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
