
class AddressModel {

    addressId: number;
    country: string;
    city: string;
    pincode: string;
    location: string;
    primaryAddress: string;
    secondaryAddress: string;
}