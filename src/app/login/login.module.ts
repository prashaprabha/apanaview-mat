import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { MaterialModule } from '../material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoginService } from './login.service';
import { LoginValidation } from './common-login-validation.service';

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule,
       MaterialModule,
       ReactiveFormsModule,
       FormsModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [LoginComponent],
    providers : [LoginService,LoginValidation]
})
export class LoginModule {}
