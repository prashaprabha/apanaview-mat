import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaComponent } from './sa.component';
import { CreateSuperAdminComponent } from './create-sa/create-sa.component';
import { EditSuperAdminComponent } from './edit-sa/edit-sa.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

const routes: Routes = [
  {
  path : "",
  component : SaComponent,
  children : [
    {
        path : "create-sa",
        component : CreateSuperAdminComponent
    }
  ]  
},
  {
    path : "create-sa",
    component : CreateSuperAdminComponent
  },
  {
    path : "edit-sa",
    component : EditSuperAdminComponent
  },
  {
    path : "**",
    component : CreateSuperAdminComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers : [Location, {provide: LocationStrategy, useClass: HashLocationStrategy}]
})
export class SaRoutingModule { }
