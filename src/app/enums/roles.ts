
export enum ROLES{
    superAdmin ="SUPER ADMIN",
    agent = "AGENT",
    supplier = "SUPPLIER",
    customer = "CUSTOMER",
    guestCustomer = "GUEST CUSTOMER"
} 