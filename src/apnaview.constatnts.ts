import { HttpParams } from '@angular/common/http';

export const URL = 'http://localhost:8080/';
//URI's for users Module
export const http_params=new HttpParams();
export const HEADERS = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
  }

/*************OAUTH2********************/
export const ACCESS_TOKEN = 'oauth/token';
export const GET_SA_USER_DETAILS: string = 'user/superadmin/user?userEmail=';
export const REFRESH_TOKEN = 'oauth/token?grant_type=refresh_token&refresh_token=';

/************** SuperAdmin ***********************/
export const SA_EMAIL_EXITS = 'user/superadmin/emailExists';
export const SA_MOBILE_EXITS = 'user/superadmin/mobileExists';
export const SA_CREATE = 'user/superadmin/create';
export const SA_GET_ALL = 'user/superadmin/getAll';
export const SA_GET = 'user/superadmin/get/';
export const SA_UPDATE = 'user/superadmin/update';
export const SA_ACTIVE_STATUS = 'user/superadmin/statusBy/';
export const SA_DELETE = 'user/superadmin/deletedBy/';