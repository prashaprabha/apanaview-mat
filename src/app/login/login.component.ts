import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SuperAdminModel } from '../models/super-admin.model';
import { ROLES } from '../enums/roles';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { OauthTokenModel } from '../models/oauth-token-model';
import { LoginService } from './login.service';
import { AlertService } from '../_alert';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
   // constructor(private router: Router) {}

   // ngOnInit() {}

    // onLogin() {
    //     localStorage.setItem('isLoggedin', 'true');
    //     this.router.navigate(['/dashboard']);
    // }

    loginForm: FormGroup;
    submitted = false;
    passwordHide = true;
    oauthTokenModel: OauthTokenModel;
    saSideNav= false;
    constructor(private formBuilder: FormBuilder,private router: Router, private logInService: LoginService, private alertService:AlertService) { }
  
    ngOnInit() {
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]]
    });
    }
  
    get f() { return this.loginForm.controls; }
  
    onSubmit() {
      this.submitted = true;
      this.loginForm.controls['email'].setValue('prashaprabha@gmail.com');
      this.loginForm.controls['password'].setValue('test123');
      if (this.loginForm.valid) {
        this.logInService.superAdminLogin(this.loginForm.value).subscribe((data) => {
                this.oauthTokenModel = data;
                let saUserModel = new SuperAdminModel();
                this.logInService.getUserDetails(this.loginForm.controls['email'].value,this.oauthTokenModel.access_token).subscribe((userInfo)=>{
                  if (sessionStorage.getItem(ROLES.superAdmin)) {
                    sessionStorage.removeItem(ROLES.superAdmin);
                  }
                  saUserModel = userInfo;
                  sessionStorage.setItem(ROLES.superAdmin, JSON.stringify(userInfo));
                  sessionStorage.setItem("sa-oauthTokenModel", JSON.stringify(data));
                sessionStorage.setItem("sa-token", this.oauthTokenModel.access_token);
                localStorage.setItem('isLoggedin', 'true');
                this.alertService.success("Logged In Successfully");
                this.router.navigate(['/dashboard']);
                },
                error=>{
                  console.log(error);
                  this.alertService.error("Server error while Super admin user details");
                  this.router.navigate(['/']);
                });
                
        },
        error=>{
          console.log(error);
          this.alertService.error("Please Provide Valid Credentials");
          this.router.navigate(['/login']);
        });
      }else{
          this.alertService.warn("Invalid Credentials");
      };
    }
  
}
