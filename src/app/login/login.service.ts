import { ROLES } from './../enums/roles';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { OauthTokenModel } from '../models/oauth-token-model';
import { User } from '../models/user';
import { SuperAdminModel } from '../models/super-admin.model';
import { ACCESS_TOKEN } from 'src/apnaview.constatnts';
import { Observable, observable } from 'rxjs';


 
@Injectable()
export class LoginService{

    oauthTokenModel: OauthTokenModel;
    
    constructor(private httpClient:HttpClient){

    }


    superAdminLogin(user: User): Observable<OauthTokenModel> {
            const body = new HttpParams()
            .set('username', user.email)
            .set('password', user.password)
            .set('grant_type', 'password');
            const headers = {
                'Authorization': 'Basic ' + btoa('apnaview-superadmin-client:apnaview-secret'),
                'Content-type': 'application/x-www-form-urlencoded',
                'Access-Control-Allow-Origin':'*'
              }
    
         return this.httpClient.post<OauthTokenModel>(ACCESS_TOKEN, body, { headers: headers }).pipe(map(res=>res),catchError(this.handleErrorObservable));
      }


      getUserDetails(email: string, token: string): Observable<SuperAdminModel> {
//export const GET_SA_USER_DETAILS: string = 'apnaview/superadmin/user?userEmail=';
        return   this.httpClient.get<SuperAdminModel>("http://localhost:8080/user/superadmin/user?userEmail="+email+"&access_token="+token).pipe(map(res=>res),catchError(this.handleErrorObservable));
      }
          
      saLogOut(): boolean{
          if(sessionStorage.getItem(ROLES.superAdmin)){
            sessionStorage.removeItem(ROLES.superAdmin);
            sessionStorage.removeItem("sa-oauthTokenModel");
            sessionStorage.removeItem("sa-token");
          }
          return true;
         }

      handleErrorObservable (error: HttpErrorResponse | any) 
      {
          console.log(error.headers.get('X-Token'));
          let myHeader = error.headers.get('my-header');
          console.log("header"+myHeader);
          console.log(error.message || error);
          //console.log("Error in Observable");
          return Observable.throw(error.message || error || "some error occured");
      }

      extractData(res: Response) 
    {
       
        return res ;
    }
    
}