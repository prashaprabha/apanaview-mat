import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSaComponent } from './edit-sa.component';

describe('EditSaComponent', () => {
  let component: EditSaComponent;
  let fixture: ComponentFixture<EditSaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
