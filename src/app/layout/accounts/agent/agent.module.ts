import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { AgentComponent } from './agent.component';
import { AgentRoutingModule } from './agent-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    AgentRoutingModule
  ],
  declarations: [AgentComponent]
})
export class AgentModule { }
