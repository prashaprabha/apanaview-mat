export class AgencyModel
{
    
    agencyId:number;
    parentAgencyId:number;
    userId:number
    agencyName:string;
    userFName:string;
    userLName:string;
    userName:string;
    userEmail:string;
    userPhomeNO:number;
    userType:string;// Agent or Subagent
    userCity:string;
}
