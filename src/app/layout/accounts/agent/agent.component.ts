import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { AgencyModel } from 'src/app/models/agency-model';

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.scss']
})
export class AgentComponent implements OnInit {


  displayedColumns: string[] = ['agencyName', 'userFName', 'userLName', 'userEmail','userPhomeNO','userType','userCity'];
  dataSource: MatTableDataSource<AgencyModel>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  constructor() { }

  ngOnInit() {
  }

}
