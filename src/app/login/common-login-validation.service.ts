import { Injectable } from '@angular/core';

@Injectable()
export class LoginValidation{

    constructor(){

    }

    validateEmail(email){
        return email.hasError('required') ? 'You must enter a value' :
                email.hasError('email') ? 'Not a valid email' :'';
    }

}