import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { SuperAdminModel } from 'src/app/models/super-admin.model';
import { HEADERS } from 'src/apnaview.constatnts';

@Injectable({
  providedIn: 'root'
})
export class SaService {


    sa_token: string;
    constructor(private httpCleint: HttpClient){
        this.sa_token = sessionStorage.getItem("sa-token");
    }

    public emailExists(email: string): Observable<boolean>{
      
        return this.httpCleint.get<boolean>("").pipe(map(res=>res),catchError(this.handleErrorObservable));
    }
    public mobileExists(){
        
    }
    public create(){

    }

    getSaUsersList():Observable<SuperAdminModel[]>{
        let params = new HttpParams();
        params.set("access_token",this.sa_token);
        // SA_GET_ALL ,catchError(this.handleErrorObservable)
        return this.httpCleint.get<SuperAdminModel[]>("http://localhost:8080/user/superadmin/getAll?access_token="+this.sa_token, { headers: HEADERS}).pipe(map(res=>res));
      }

    public get(){

    }

    public update(){

    }

    public activeStatus(){

    }
    public deleteStatus(){

    }


    handleErrorObservable (error: HttpErrorResponse | any) 
    {
        console.log(error.headers.get('X-Token'));
        let myHeader = error.headers.get('my-header');
        console.log("header"+myHeader);
        console.log(error.message || error);
        //console.log("Error in Observable");
        return Observable.throw(error.message || error || "some error occured");
    }
    
}