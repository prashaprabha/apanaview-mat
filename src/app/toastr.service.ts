

// import { Injectable } from '@angular/core';

// @Injectable()
// export class ToastrService{

//     constructor(public toastr: ToastrManager) {}
 
//     showSuccess(message:string) {
//         this.toastr.successToastr(message, 'Success!',{timeout:10});
//     }

//     showError(message:string) {
//         this.toastr.errorToastr(message, 'Oops!',{timeout:10});
//     }

//     showWarning() {
//         this.toastr.warningToastr('This is warning toast.', 'Alert!');
//     }

//     showInfo() {
//         this.toastr.infoToastr('This is info toast.', 'Info');
//     }

//     showCustom() {
//         this.toastr.customToastr(
//         "<span style='color: green; font-size: 16px; text-align: center;'>Custom Toast</span>",
//         null,
//         { enableHTML: true }
//         );
//     }

//     showToast(position: any = 'top-left') {
//         this.toastr.infoToastr('This is a toast.', 'Toast', {
//             position: position
//         });
//     }

//     showAnimate(animate: any = 'slideFromRight') {
//         this.toastr.infoToastr('This is a toast.', 'Toast', {
//             animate: animate, toastTimeout: 300, dismiss: 'click'
//         });
//     }

// }