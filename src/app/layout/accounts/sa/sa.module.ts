import { NgModule } from '@angular/core';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { SaRoutingModule } from './sa-routing.module';
import { SaComponent } from './sa.component';
import { SaService } from './sa.service';
import { MaterialModule } from 'src/app/material.module';
import { EditSuperAdminComponent } from './edit-sa/edit-sa.component';
import { CreateSuperAdminComponent } from './create-sa/create-sa.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SaComponent, EditSuperAdminComponent, CreateSuperAdminComponent],
  imports: [
    CommonModule,
    SaRoutingModule,
    MaterialModule,
    ReactiveFormsModule  ],
  providers : [SaService,Location, {provide: LocationStrategy, useClass: HashLocationStrategy}]
})
export class SaModule { }
