import { HttpErrorResponse } from "@angular/common/http";
import { ErrorService } from "./errorservice";
import { LoggingService } from "../logging/loggingservice";
import { Injector, Injectable, ErrorHandler } from "@angular/core";
import { AlertService } from "../_alert";

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    // Error handling is important and needs to be loaded first.
    // Because of this we should manually inject the services with Injector.
    constructor(private injector: Injector) { }

    handleError(error: Error | HttpErrorResponse) {

        const errorService = this.injector.get(ErrorService);
        const logger = this.injector.get(LoggingService);
        const alertservice = this.injector.get(AlertService);

        let message;
        let stackTrace;

        if (error instanceof HttpErrorResponse) {
            // Server Error
            message = errorService.getServerMessage(error);
            stackTrace = errorService.getServerStack(error);
            alertservice.error(message);
        } else {
            // Client Error
            message = errorService.getClientMessage(error);
            stackTrace = errorService.getClientStack(error);
            alertservice.error(message);
        }

        // Always log errors
        logger.logError(message, stackTrace);

        console.error(error);
    }
}