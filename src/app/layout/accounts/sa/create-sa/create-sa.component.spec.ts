import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSaComponent } from './create-sa.component';

describe('CreateSaComponent', () => {
  let component: CreateSaComponent;
  let fixture: ComponentFixture<CreateSaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
