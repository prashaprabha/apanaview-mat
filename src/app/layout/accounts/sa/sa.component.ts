import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource, MatPaginator, MatSort } from "@angular/material";
import { SuperAdminModel } from "src/app/models/super-admin.model";
import { SaService } from "./sa.service";
import { ROLES } from "src/app/enums/roles";
import { Router } from "@angular/router";

@Component({
  selector: 'app-sa',
  templateUrl: './sa.component.html',
  styleUrls: ['./sa.component.scss']
})
export class SaComponent implements OnInit {

  displayedColumns: string[] = ['firstName', 'lastName', 'mobileNumber', 'email','maxAdmins','organization','lastLogin'];
  dataSource: MatTableDataSource<SuperAdminModel>;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private saService:SaService, private router: Router) { }
  token: string;
  saUserModel: SuperAdminModel;
  saUsersList:SuperAdminModel[];
  ngOnInit() {
    this.saUserModel = JSON.parse(sessionStorage.getItem(ROLES.superAdmin));
     this.getSaUsersList(this.saUserModel.userId);
  }

  getSaUsersList(userId:number){
    this.saService.getSaUsersList().subscribe((data) => {
      console.log("log"+data);
      this.saUsersList = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    error=>{
      if(error.status == 302){
        this.saUsersList = error.error;
      this.dataSource = new MatTableDataSource(this.saUsersList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      }
      console.log("log"+error);
    })
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createSA(){
    this.router.navigate(['/sa/create-sa']);
  }

}

