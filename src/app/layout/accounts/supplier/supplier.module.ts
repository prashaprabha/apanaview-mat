import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupplierRoutingModule } from './supplier-routing.module';
import { SupplierComponent } from './supplier.component';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  imports: [
    CommonModule,
    SupplierRoutingModule,
    MaterialModule
  ],
  declarations: [SupplierComponent]
})
export class SupplierModule { }
