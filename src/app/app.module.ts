import { LayoutModule } from '@angular/cdk/layout';
import { OverlayModule } from '@angular/cdk/overlay';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { GlobalErrorHandler } from './exception/globalerrorhandler';
import { ServerErrorInterceptor } from './exception/servererrorinterceptor';
import { AlertModule, AlertService } from './_alert';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AuthGuard } from './shared/guard/auth.guard';
import { LoginModule } from './login/login.module';
// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-javascript/sb-admin-material/master/dist/assets/i18n/',
        '.json'
    );*/
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        LayoutModule,
        LoginModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        OverlayModule,
        HttpClientModule,
        MaterialModule,
        AlertModule,
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        })
    ],
    providers: [ { provide: ErrorHandler, useClass: GlobalErrorHandler }, 
                { provide: HTTP_INTERCEPTORS, useClass: ServerErrorInterceptor, multi: true },
    AlertService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
      ],
    bootstrap: [AppComponent]
})
export class AppModule {}
