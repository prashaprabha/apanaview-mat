
export class SuperAdminModel {
    userId: number;
    firstName: string;
    lastName: string;
    mobileNumber: string;
    landLineNumber: string;
    email: string;
    password: string;
    maxAdmins: number;
    organization: string;
    lastLogin: Date;
    gstNumber: string;
    activeStatus: number;
    deleteStatus: boolean;
    modifiedAt: Date;
    createdAt: Date;
    createdBy: number;
    //address: AddressModel;
}


